# ShareBook App

Une application permettant de partager des avis sur des livres venant d'une API Rest "Sharebook Back"

### Installation et utilisation

##### Getting Started
1. Cloner le projet
2. Installer les dépendances avec ```npm install```
3. Lancer en ligne de commandes ```npm run serve``` 

##### Important
En duo avec le projet <a href="https://gitlab.com/mel.gvn/sharebook-back">Sharebook Back</a>.
